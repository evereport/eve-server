const config = require('./config');
const express = require('express');
const utils = require('./src/utils');
const Database = require('./src/classes/DatabaseProvider');
const Run = require('./src/classes/REST/v1/Run.rest');
const EveSuite = require('./src/classes/REST/v1/EveSuite.rest');
const Controller = require('./src/classes/ResfulController');
const Handler = require('./src/SequelizeErorrsHandlers');
const cors = require('cors');
const app = express();
app.use(express.json({ limit: '10mb', extended: true }));
app.use(cors());

const database = new Database(config.database);
database.init().then(() => {
	const controller = new Controller([
		new Run(database),
		new EveSuite(database)
	]);
	app.use('/', controller.app);
	app.use(Handler);
	app.listen(config.server.port, () => {
		console.log(`Starting server on port ${config.server.port}`);
	});
});
//Healthcheck
app.get('/status', utils.healthcheck);
