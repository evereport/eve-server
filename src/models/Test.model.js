const {Model, DataTypes} = require('sequelize');
/**
 * @external Sequelize
 * @param {Sequelize} sequelize
 */
module.exports = (sequelize) => {
	/**
	 * @extends {Model}
	 * @class
	 */
	const Test = class Test extends Model {};

	Test.init({
		id: { //Unique id of test
			type: DataTypes.UUID,
			primaryKey: true,
		},
		parentId: {
			allowNull: true,
			type: DataTypes.UUID,
		},
		name: {
			allowNull: false,
			type: DataTypes.TEXT,
		},
		run_id: {
			allowNull: false,
			type: DataTypes.UUID
		},
		config: {
			allowNull: false,
			type: DataTypes.JSON
		},
		error: {
			allowNull: true,
			type: DataTypes.JSON
		},
		state: {
			allowNull: false,
			type: DataTypes.ENUM('PASSED', 'FAILED', 'PENDING')
		}
	}, {
		sequelize,
		modelName: "test-record",
		tableName: 'test-record',
		timestamps: false
	});
	return Test;
};
