const {Model, DataTypes} = require('sequelize');
/**
 * @external Sequelize
 * @param {Sequelize} sequelize
 */
module.exports = (sequelize) => {
/**
 * @extends {Model}
 * @class
 */
	const Suite = class Suite extends Model {};

	Suite.init({
		id: { //Unique id of test
			type: DataTypes.UUID,
			primaryKey: true,
		},
		parentId: {
			type: DataTypes.UUID,
		},
		run_id: {
			allowNull: false,
			type: DataTypes.UUID
		},
		name: {
			allowNull: false,
			type: DataTypes.TEXT,
		},
		config: {
			allowNull: false,
			type: DataTypes.JSON
		},
		errors: {
			allowNull: true,
			type: DataTypes.JSON
		}
	}, {
		sequelize,
		modelName: "suite-record",
		tableName: 'suite-record',
		timestamps: false,
		hierarchy: {
			childrenAs: 'suites'
		}
	});
	return Suite;
};
