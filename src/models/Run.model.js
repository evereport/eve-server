const {Model, DataTypes} = require('sequelize');
/**
 * @external Sequelize
 * @param {Sequelize} sequelize
 */
module.exports = (sequelize) => {
	/**
	 * @extends {Model}
	 * @class
	 */
	const Run = class Run extends Model {};

	Run.init({
		id: {
			type: DataTypes.UUID,
			primaryKey: true
		},
		name: {
			allowNull: false,
			type:DataTypes.TEXT
		},
		error: {
			allowNull: true,
			type: DataTypes.JSON
		},
		errors: {
			allowNull: true,
			type: DataTypes.JSON
		},
		created: {
			allowNull: false,
			type: DataTypes.DATE,
			defaultValue: DataTypes.NOW
		},
		fatal: {
			allowNull: true,
			defaultValue: false,
			type: DataTypes.BOOLEAN
		},
		config: {
			allowNull: false,
			type: DataTypes.JSON
		},
		finished: {
			allowNull: false,
			defaultValue: false,
			type: DataTypes.BOOLEAN
		}
	}, {
		sequelize,
		modelName: "run-record",
		tableName: 'run-record',
		timestamps: false,
	});

	return Run;
};
