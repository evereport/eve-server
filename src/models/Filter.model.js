const {Model, DataTypes} = require('sequelize');
/**
 * @external Sequelize
 * @param {Sequelize} sequelize
 */
module.exports = (sequelize) => {
	/**
	 * @extends {Model}
	 * @class
	 */
	const Filter = class Suite extends Model {};

	Filter.init({
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			primaryKey: true
		},
		scope: {
			allowNull: true,
			type: DataTypes.ARRAY(DataTypes.TEXT)
		},
		epic: {
			allowNull: true,
			type: DataTypes.TEXT
		},
		feature: {
			allowNull: true,
			type: DataTypes.TEXT
		},
		story: {
			allowNull: true,
			type: DataTypes.TEXT
		},
		project_id: {
			allowNull: true,
			type: DataTypes.INTEGER
		}
	}, {
		sequelize,
		modelName: "filter",
		tableName: 'filter',
		timestamps: false,
	});

	return Filter;
};

