const joi = require("@hapi/joi");
const Filter = require('./Filter.schema');
const Config = require('./Config.schema');
const { uuid } = require('./common');
const State = require('./State.schema');

module.exports = joi.object({
	id: uuid.required(),
	title: joi.string().required(),
	state: State.required(),
	parentId: uuid,
	runId: uuid.required(),
	filter: Filter.required(),
	config: Config.required(),
	error: joi.object().unknown(true),
	timing: joi.number().required()
}).unknown(true);