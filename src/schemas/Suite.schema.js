const joi = require("@hapi/joi");
const Test = require('./Test.schema');
const Filter = require('./Filter.schema');
const Config = require('./Config.schema');
const Stats = require('./Stats.schema');
const { uuid } = require('./common');
const State = require('./State.schema');

module.exports = joi.object({
	id: uuid.required(),
	title: joi.string().required(),
	state: State.required(),
	parentId: uuid,
	runId: uuid.required(),
	filter: Filter.required(),
	config: Config.required(),
	suites: joi.array().items(joi.link('...')),
	tests: joi.array().items(Test),
	stats: Stats.required(),
	timing: joi.number().required()
}).unknown(true);


