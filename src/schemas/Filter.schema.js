const joi = require("@hapi/joi");
const {stringArr} = require('./common');

module.exports = joi.object({
	scope: stringArr,
	epic: stringArr,
	feature: stringArr,
	story: stringArr,
	project_id: joi.number().integer().allow(null)
});