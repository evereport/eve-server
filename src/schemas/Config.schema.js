const joi = require("@hapi/joi");
const { boolean } = require('./common');

module.exports = joi.object({
	timeout: joi.number().integer(),
	parallel: boolean,
	step: boolean,
	skip: boolean
});