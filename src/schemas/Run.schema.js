const joi = require("@hapi/joi");
const Filter = require('./Filter.schema');
const Suite = require('./Suite.schema');

module.exports = joi.object({
	id: joi.string().guid({ version: "uuidv4" }).required(),
	name: joi.string().required(),
	error: joi.string(),
	errors: joi.string().required(),
	config: joi.string().required(),
	fatal: joi.boolean(),
	finished: joi.boolean(),
	filter: Filter,
	suites: joi.array().items(Suite)
});
