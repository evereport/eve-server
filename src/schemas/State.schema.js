const joi = require('@hapi/joi');
const { states } = require('../utils');

const state = joi.string().valid(...Object.values(states));

module.exports = state;
