const joi = require("@hapi/joi");
const { states } = require('../utils');

const stat = joi.object({
	[states.PASSED]: joi.number().integer(),
	[states.FAILED]: joi.number().integer(),
	[states.BROKEN]: joi.number().integer(),
	[states.PENDING]: joi.number().integer(),
});

module.exports = joi.object({
	own: stat.required(),
	summary: stat.required(),
});
