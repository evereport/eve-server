const joi = require("@hapi/joi");

module.exports.uuid = joi.string().guid({ version: "uuidv4" }).required();
module.exports.boolean = joi.string().allow("true", "false");
module.exports.stringArr = joi.array().items(joi.string()).min(0).allow(null);
