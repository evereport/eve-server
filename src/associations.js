/**
 * @description
 * Creates associations within models
 * @external Sequelize
 * @param {object.<string, Sequelize.Model>} models
 * @param {Function} mk - sequelize-embed mk helper function
 */
module.exports = (models, mk) => {
	//Filter associations
	// Any -> Filter (Any has only one Filter)
	models.Test.Filter = models.Test.belongsTo(models.Filter);
	models.Suite.Filter = models.Suite.belongsTo(models.Filter);
	models.Run.Filter = models.Run.belongsTo(models.Filter);
	models.Seed.Filter = models.Seed.belongsTo(models.Filter);
	//Suite
	//Suite -> [Test] (One suite has many tests)
	models.Suite.Test = models.Suite.hasMany(models.Test, { sourceKey: "id", foreignKey: "parentId", as: "tests" });
	//Test -> Suite (One test belongs to only one suite)
	models.Test.Suite = models.Test.belongsTo(models.Suite, { foreignKey: "parentId", targetKey: "id", as: "suite" });
	//Seeder
	//Seeder -> [Seed] (One seeder has many seeds)
	models.Seeder.Seed = models.Seeder.hasMany(models.Seed, { sourceKey: "id", foreignKey: "parentId", as: "seeds" });
	//Seed -> Seeder (One seed belongs to only one seeder)
	models.Seed.Sedeer = models.Seed.belongsTo(models.Seeder, { foreignKey: "parentId", targetKey: "id", as: "seeder" });
	//EveSuite
	//EveSuite -> Suite (One evesuite has only one suite (actually evesuite is suite, but in db it's easier to do with assoc))
	models.EveSuite.Suite = models.EveSuite.belongsTo(models.Suite, { foreignKey: "id", targetKey: "id", as: 'suite' });
	//EveSuite -> Seeder (One evesuite has only one seeder)
	models.EveSuite.Seeder = models.EveSuite.belongsTo(models.Seeder);
	//Run
	//Run -> Seeder (One run has only one seeder)
	models.Run.Seeder = models.Run.belongsTo(models.Seeder);
	//Run -> [EveSuite] (One run has many evesuites)
	models.Run.EveSuite = models.Run.hasMany(models.EveSuite, { sourceKey: "id", foreignKey: "run_id", as: "suites" });
	//EveSuite -> Run (One evesuite belongs to only one run)
	models.EveSuite.Run = models.EveSuite.belongsTo(models.Run, { foreignKey: "run_id", targetKey: "id", as: "run" });
	/*
	//Suite -> Run (One suite belongs to only one run)
	models.Suite.Run = models.Suite.belongsTo(models.Run, { foreignKey: "run_id", targetKey: "id", as: "run" });
	//Seed -> Run (One seed belongs to only one run)
	models.Seed.Run = models.Seed.belongsTo(models.Run, { foreignKey: "run_id", targetKey: "id", as: "run" });
	//Test -> Run (One test belongs to only one run)
	models.Test.Run = models.Test.belongsTo(models.Run, { foreignKey: "run_id", targetKey: "id", as: "run" });
	*/

	const Test = () => [
		mk(
			models.Test.Filter, //Test.Filter
		)
	];
	const Suite = () => [
		mk(models.Suite.Filter),
		mk(models.Suite.associations.suites, models.Suite.Filter, mk(models.Suite.Test, ...Test())),
		mk(models.Suite.Test, ...Test())
	];
	const Seed = () => [
		mk(
			models.Seed.Filter,
		)
	];
	const Seeder = () => [
		mk(
			models.Seeder.Seed,
			...Seed()
		)
	];
	const EveSuite = () => [
		mk(models.EveSuite.Seeder, ...Seeder()),
		mk(models.EveSuite.Suite, ...Suite())
	];
	const Run = () => [
		mk(models.Run.Filter),
		mk(models.Run.Seeder, ...Seeder()),
		mk(models.Run.EveSuite, ...EveSuite())
	];
	return {
		Run: Run(),
		EveSuite: EveSuite(),
		Suite: Suite(),
		Test: Test(),
		Seeder: Seeder(),
		Seed: Seed()
	}
};