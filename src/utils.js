module.exports.healthcheck = (req, res) => {
	res.status(200).end();
};

module.exports.types = {
	EVE_RUNNER: 'EVE_RUNNER',
	SEED: 'SEED',
	EVE_SUITE: 'EVE_SUITE',
	TEST_SUITE: 'TEST_SUITE',
	TEST: 'TEST'
}
;
module.exports.states = {
	PASSED: 'PASSED',
	FAILED: 'FAILED',
	PENDING: 'PENDING',
	BROKEN: 'BROKEN'
};
