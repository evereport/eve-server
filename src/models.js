module.exports.Test = require('./models/Test.model');
module.exports.Filter = require('./models/Filter.model');
module.exports.Suite = require('./models/Suite.model');
module.exports.Seed = require('./models/Seed.model');
module.exports.Seeder = require('./models/Seeder.model');
module.exports.EveSuite = require('./models/EveSuite.model');
module.exports.Run = require('./models/Run.model');
