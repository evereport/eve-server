const Selection = require('./Selection');
const Restful = require('./Restful');
const express = require('express');
/**
 * @description
 * Collects all specified restful entities and maps them on single express application
 * @class
 */
module.exports = class RestfulController {
	/**
	 * @description
	 * Express application/router
	 * @type {express}
	 */
	app;
	/**
	 * @param {Array.<Restful>} entities - Entities
	 */
	constructor(entities) {
		this.app = express();
		entities.forEach(this.mapEntity);
	}

	/**
	 * @description
	 * Adds entity handlers to application
	 * @param {Restful} entity - Restful entity
	 * @throws {TypeError} - If entity param is not instance of Restful
	 */
	mapEntity = (entity) => {
		if (!(entity instanceof Restful))
			throw new TypeError('Entity should be Restful');
		for (const method in this.constructor.methods) {
			const fn = this.constructor.methods[method];
			if (entity[method] instanceof Selection) {
				const handlers = entity[method].get();
				for (const path in handlers) {
					if (path !== 'default')
						this.app[fn](path, handlers[path])
				}
			} else {
				this.app[fn](`/${entity.constructor.name}/:id`, entity[method])
			}
		}
	};

	/**
	 * @enum
	 */
	static methods = {
		get: 'get',
		create: 'post',
		update: 'put',
		remove: 'delete'
	}

};