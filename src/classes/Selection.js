/**
 * @description
 * Used for define several handlers on request with type
 * @class
 */
module.exports = class Selection {
	object;
	/**
	 * @param {object.<string, Function>} selection - Object with path in key and express handler in value
	 */
	constructor(selection) {
		if (!Object.keys(selection).every(f => f === 'default' || selection[f] instanceof Function))
			throw new TypeError("Selection values should be a functions");
		this.object = Object.freeze(selection);
	}

	get = () => this.object;
	default = () => this.object[this.object.default];
};