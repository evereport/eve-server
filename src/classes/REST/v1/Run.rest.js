const Restful = require('../../Restful');
const Selection = require('../../Selection');
const schema = require('../../../schemas/Run.schema');
const errors = require('../../../errors');

/**
 * @description
 * Restful data provider for Run entity
 * @class
 * @augments Restful
 */
module.exports = class Run extends Restful {
	/**
	 * @override
	 * @see Restful
	 */
	constructor(database) {
		super(database);
	}

	/**
	 * @description
	 * Returns run result
	 * @override
	 */
	get = new Selection({
		'/run/:id': (req, res, next) => {
			this.model.findByPk(req.params.id, {
				plain: true,
				include: this.database.associations.Run
			}).then((result) => {
				res.json(result).end();
			}).catch(next);
		},
		'/run': (req, res, next) => {
			this.model.findAll({
				limit: req.params.limit,
				offset: req.params.offset,
			}).then((result) => {
				res.json((result instanceof Array) ? result : [result]).end();
			}).catch(next)
		}
});

	/**
	 * @description
	 * Creates run entry
	 * @override
	 * @async
	 */
	create = new Selection({
		/*
			TODO validations:
				- Schema (done)
				- Entities id's
				- Tree filtering
		*/
		'/run':  async (req, res, next) => {
			const error = schema.validate(req.body).error;
			if (error) {
				next(error);
			} else { //TODO transaction
				this.database.sequelize.transaction((transaction) => {
					return this.model.create(req.body, {
						include: this.database.associations.Run,
						transaction: transaction
					})
				}).then(() => {
					res.status(200).end();
				}).catch((err) => {
					next(err);
				})
			}
		},
		default: '/run'
	});

/**
	 * @override
	 * @see {Restful}
	 */
	static model = "Run";
	/**
	 * @override
	 * @see {Restful}
	 */
	static name = "run";


};