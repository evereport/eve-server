const Sequelize = require('sequelize');
require('sequelize-hierarchy')(Sequelize);
const embed = require('sequelize-embed');
const models = require('./../models');
const associations = require('./../associations');

/**
 * @description
 * Database operator
 * @class
 */
module.exports = class Database {
	/**
	 * @description
	 * Database config
	 * @type {object}
	 */
	config = {};
	/**
	 * @description
	 * Sequelize instance
	 * @type {Sequelize}
	 */
	sequelize;
	/**
	 * @description
	 * Contains all sequelize models
	 * @type {object.<string, Sequelize.Model>}
	 */
	models = {};
	embed;

	constructor(config) {
		this.config = config;
		this.sequelize = new Sequelize(config.database, config.username, config.password, {
			host: config.host,
			port: config.port,
			schema: config.schema,
			logging: config.logging,
			dialect: 'postgres'
		});
		this.embed = embed(this.sequelize);
		this.mk = this.embed.util.helpers.mkInclude;
		this._importModels();
		this.associations = associations(this.models, this.mk);
	}

	/**
	 * @description
	 * Initializes schema and tables if not exists
	 * @async
	 */
	init = async () => {
		await this.sequelize.createSchema(this.config.schema).catch((error) => {
			if (error instanceof Sequelize.DatabaseError) {
				if (error.message === `schema "${this.config.schema}" already exists`) {
					console.log('Create schema failed, well it\'s because schema already exists, skipping...')
				} else throw error;
			} else throw error;
		});
		await this.sequelize.sync();
	};

	/**
	 * @description
	 * Imports sequelize models into current sequelize instance
	 * @private
	 */
	_importModels = () => {
		for (const key in models)
			this.models[key] = models[key](this.sequelize);
	}

};

