/**
 * @class
 * @abstract
 */
module.exports = class Restful {
	/**
	 * @external DatabaseProvider
	 * @type {DatabaseProvider}
	 * @public
	 */
	database;
	/**
	 * @external Sequelize
	 * @type {Sequelize.Model}
	 * @public
	 */
	model;
	/**
	 * @external DatabaseProvider
	 * @param {DatabaseProvider} database - Database provider
	 */
	constructor(database) {
		this.database = database;
		this.model = database.models[this.constructor.model];
	}

	/**
	 * @description
	 * Express handler for GET request on this model
	 * @param {object} req - Request
	 * @param {object} res - Response
	 * @param {Function} next - Next function (if middleware)
	 */
	get = (req, res, next) => {
		throw new ReferenceError('GET is not implemented');
	};

	/**
	 * @description
	 * Express handler for POST request on this model
	 * @param {object} req - Request
	 * @param {object} res - Response
	 * @param {Function} next - Next function (if middleware)
	 */
	create = (req, res, next) => {
		throw new ReferenceError('POST is not implemented');
	};

	/**
	 * @description
	 * Express handler for PUT request on this model
	 * @param {object} req - Request
	 * @param {object} res - Response
	 * @param {Function} next - Next function (if middleware)
	 */
	update = (req, res, next) => {
		throw new ReferenceError('POST is not implemented');
	};

	/**
	 * @description
	 * Express handler for DELETE request on this model
	 * @param {object} req - Request
	 * @param {object} res - Response
	 * @param {Function} next - Next function (if middleware)
	 */
	remove = (req, res, next) => {
		throw new ReferenceError('DELETE is not implemented');
	};


	/**
	 * @description
	 * Model name
	 * @type {string}
	 */
	static model;
	/**
	 * @description
	 * Restful entity name
	 * @type {string}
	 */
	static name;

};