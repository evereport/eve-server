module.exports = (err, req, res, next) => {
	if (module.exports[err.name]) {
		const error = module.exports[err.name](err);
		res.status(error.status).json(error.error).end();
	} else {
		res.json(module.exports.UnknownError(err)).end();
	}
};

module.exports.SequelizeUniqueConstraintError = (err) => {
	const fields = err.fields;
	return {
		status: 400,
		error: {
			message: `Some of keys already exists in ${err.original.table}. If you were trying to modify existing record, please use PUT method.`,
			code: "EveEntityAlreadyExists",
			fields
		}
	}
};

module.exports.ValidationError = (err) => {
	const details = err.details;
	return {
		status: 400,
		error: {
			message: "Entity validation failed.",
			details
		}
	}
};

module.exports.ErrorEntityNotFound = (err) => {
	return {
		status: 404,
		error: {
			message: err.message
		}
	}
};

module.exports.ErrorReadonlyEntity = (err) => {
	return {
		status: 403,
		error: {
			message: err.message
		}
	}
};

module.exports.UnknownError = (err) => {
	console.log(err); //TODO remove this kinda log
	return {
		status: 500,
		error: {
			code: 'UnknownError',
			message: 'Unknown error. Log and stacktrace saved.'
		}
	};
};


module.exports.a = 1;