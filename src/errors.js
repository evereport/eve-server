module.exports.EveError = class EveError extends Error { name = "EveError"; };


module.exports.ErrorEntityNotFound = class ErrorEntityNotFound extends module.exports.EveError {
	constructor(entity, id) {
		super();
		this.name = "ErrorEntityNotFound";
		this.message = `There is no ${entity} with ${id}`
	}
};

module.exports.ErrorReadonlyEntity = class ErrorReadonlyEntity extends module.exports.EveError {
	constructor(entity, id, run_id) {
		super();
		this.name = "ErrorReadonlyEntity";
		this.message = `${entity} with ${id} belongs to result (${run_id}) which is complete and now readonly`;
	}
};