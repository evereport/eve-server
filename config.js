module.exports = {
	server: {
		port: 3001
	},
	database: {
		host: process.env.POSGRES_HOST || 'localhost',
		port: process.env.POSTGRES_PORT || '5432',
		database: process.env.POSTGRES_DB || 'eve',
		username: process.env.POSTGRES_USER || 'eve',
		password: process.env.POSTGRES_PASSWORD ||'password',
		schema: process.env.POSTGRES_SCHEMA || 'eve',
		logging: true
	}
};